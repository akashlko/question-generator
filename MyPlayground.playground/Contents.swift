import UIKit

class Question {
    var type: QuestionType
    var query: String
    var answer: String
    
    init(type: QuestionType, query: String, answer: String) {
        self.type = type
        self.query = query
        self.answer = answer
    }
}
    
enum QuestionType: String {
    case trueFalse = "The color of sky is blue"
    case multipleChoice = "Who is most stupid CM of India: Kejriwal, Rahul Gandhi, Akhilesh Yadav, Lalu? "
    case shortAnswer = "what is capital of india?"
    case essay = "Explain molecular fusion"

    static let types = [trueFalse, multipleChoice, shortAnswer, essay]
}
 
enum AnswerType: String {
    case trueFalse = "True"
    case multipleChoice = "Kejriwal."
    case shortAnswer = "Delhi"
    case essay = "When more than one electrons contact"
    
    static let types = [trueFalse, multipleChoice, shortAnswer, essay]

}

protocol QuestionGenerator {
    func generateRandomQuestion() -> Question
}

class Quiz: QuestionGenerator {
    func generateRandomQuestion() -> Question{
        let randomNumeral = Int(arc4random_uniform(4))
        let randomType = QuestionType.types[randomNumeral]
        let randomQuery = randomType.rawValue
        let randomAnswer = AnswerType.types[randomNumeral].rawValue
        let randomQuestion = Question(type: randomType, query: randomQuery, answer: randomAnswer)
        return randomQuestion
    }
}

let quiz = Quiz()
let question = quiz.generateRandomQuestion()
print("Question Type: \(question.type) \n Query: \(question.query) \n Answer: \(question.answer)")

